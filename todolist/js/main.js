let completedArr = [];
let toDoArr = [];
let sortedArr = [];

const addItem = () => {
  let inputEle = document.getElementById(`newTask`).value;
  toDoArr.push(inputEle);
  renderToDoList(toDoArr);
  return toDoArr;
};

const removeItem = (item) => {
  let indexDelete = toDoArr.indexOf(item);
  toDoArr.splice(indexDelete, 1);
  renderToDoList(toDoArr);
};

const completedItem = (item) => {
  let completeHTML = ``;
  completedArr.push(item);
  completedArr.forEach((i) => {
    completeHTML += `
      <li>
          ${i}
          <div class="buttons">
              <button>
                  <i class="fa fa-trash-alt"></i>
              </button>
              <button class="completed">
                  <i class="fa fa-check-circle"></i>
              </button>
          </div>
      </li>
    `;
  });
  document.getElementById(`completed`).innerHTML = completeHTML;
};

const renderToDoList = (toDoArr) => {
  let contentHTML = ``;
  toDoArr.forEach((e) => {
    contentHTML += `
      <li>
          ${e}
          <div class="buttons" onclick="removeItem('${e}')">
              <button class="remove">
                  <i class="fa fa-trash-alt"></i>
              </button>
              <button class="complete" onclick="completedItem('${e}')">
                  <i class="fa fa-check-circle"></i>
              </button>
          </div>
      </li>
    `;
  });
  document.getElementById(`todo`).innerHTML = contentHTML;
  document.getElementById(`newTask`).value = ``;
};

const sortItemAToZ = () => {
  toDoArr.forEach((i) => {
    i.toLowerCase();
  });
  toDoArr.sort((a, b) => a.localeCompare(b));
  renderToDoList(toDoArr);
};

const sortItemZToA = () => {
  toDoArr.forEach((i) => {
    i.toLowerCase();
  });
  toDoArr.reverse((a, b) => a.localeCompare(b));
  renderToDoList(toDoArr);
};

window.sortItemAToZ = sortItemAToZ;
window.sortItemZToA = sortItemZToA;
window.removeItem = removeItem;
window.completedItem = completedItem;
window.addItem = addItem;
